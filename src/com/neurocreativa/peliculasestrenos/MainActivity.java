package com.neurocreativa.peliculasestrenos;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Esta aplicaci�n descarga las peliculas RSS de www.tucamon.es y las muestra. 
 * Fue desarrollada en el taller de Android llevado a cabo en el aula de CAMON 
 * de Alicante en enero de 2011, por los asistentes y por Boy�n Bonev 
 * (Universidad de Alicante) Esta aplicaci�n no es \"oficial\" de CAMON, no se 
 * puede vender y su finalidad es did�ctica. La Universidad de Alicante y CAMON 
 * no se responsabilizan de su uso y modificaciones hechas por terceros.
 * 
 * @author Boyan Bonev, Universidad de Alicante
 *
 */
public class MainActivity extends Activity {
	
	private static String URL_BASE;
	private static String URL_RSS;

	Context context;
	ListView listView;
	
	ArrayList<Pelicula> peliculas;
	PeliculasAdapter peliculasAdapter;
	TareaDescarga tarea;
	Speech speech;

	
    /** Called when the activity is first created. */
    @SuppressWarnings("unchecked")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();

        URL_BASE = getString(R.string.url_base);
        URL_RSS = getString(R.string.url_rss);

        final ArrayList<Pelicula> data = (ArrayList<Pelicula>) getLastNonConfigurationInstance();
        if (data == null) {
            peliculas = new ArrayList<Pelicula>();
            peliculasAdapter = new PeliculasAdapter(context, R.layout.fila, peliculas);
            lanzaDescargaDePeliculas();
        }else{
        	peliculas = data;
            peliculasAdapter = new PeliculasAdapter(context, R.layout.fila, peliculas);
        }

        speech = new Speech(this);
        
        listView = (ListView)findViewById(R.id.ListView01);
        listView.setAdapter(peliculasAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				Pelicula n = peliculas.get((int)id);
				//PeliculaAlertDialog nad = new PeliculaAlertDialog(MainActivity.this, speech, n);				
				//nad.show();
		   		Intent intent = new Intent(context, PeliculaActivity.class);
	    		startActivity(intent);
			}
		});
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        final ArrayList<Pelicula> data = peliculas;
        return data;
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	speech.installOrSpeak(requestCode,resultCode);
    	
    }
    
    @Override
    public void onStop() {
    	speech.stop();
    	super.onStop();
    }

    @Override
	public boolean onCreateOptionsMenu(Menu m) {
		getMenuInflater().inflate(R.menu.main, m);
		return true;
	}
    
 	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.item01:
			speech.stop();
			break;
		case R.id.item02:
			lanzaDescargaDePeliculas();
			break;
		case R.id.item03:
			AlertDialog.Builder ab=new AlertDialog.Builder(MainActivity.this);
			ab.setTitle(R.string.acerca_de_camon);
			ab.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
			ab.setMessage(R.string.licencia);
			ab.setPositiveButton(R.string.aceptar,null);
			ab.show();
			break;
		}
		return true;
	}
    
    
    void lanzaDescargaDePeliculas(){
		try {
			tarea = new TareaDescarga();
			tarea.execute(new URL(URL_RSS));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
   }
   
   
   private class TareaDescarga extends AsyncTask<URL, String, List<Pelicula>>{
	    ArrayList<Pelicula> peliculasDescargadas;
		ProgressDialog progressDialog;
		boolean error=false;

		@Override
		protected List<Pelicula> doInBackground(URL... params) {
			try {
				URL url = params[0];
				peliculasDescargadas = new ArrayList<Pelicula>();
				
				XmlPullParserFactory parserCreator = XmlPullParserFactory
						.newInstance();
				XmlPullParser parser = parserCreator.newPullParser();
				parser.setInput(url.openStream(), null);
				int parserEvent = parser.getEventType();
				int nItems = 0;
				while (parserEvent != XmlPullParser.END_DOCUMENT) {
					switch (parserEvent) {
					case XmlPullParser.START_TAG:
						String tag = parser.getName();
						if (tag.equalsIgnoreCase("item")) {
							publishProgress(getString(R.string.descargandopelicula)+" "+(++nItems));
							Pelicula pelicula = new Pelicula();
							parserEvent = parser.next();
							boolean itemClosed = false;
							while (parserEvent != XmlPullParser.END_DOCUMENT && !itemClosed) {
								switch (parserEvent) {
								case XmlPullParser.START_TAG:
									tag = parser.getName();
									if (tag.equalsIgnoreCase("title")) {
										pelicula.setTitulo(parser.nextText());
									}
									if (tag.toLowerCase().contains("pubdate")) {
										pelicula.setFecha(parser.nextText());
									}
									if (tag.equalsIgnoreCase("link")) {
										pelicula.setLink(URL_BASE+parser.nextText());
									}
									if (tag.equalsIgnoreCase("description")) {
										String textoHtml = parser.nextText();
										Spanned texto = Html.fromHtml(textoHtml, new ImageGetter(), null);
										
										pelicula.setDescripcion(texto);
	
										String linkImagen = URL_BASE+imageSource;
										pelicula.setLinkImagen(linkImagen);
									}
									break;
								case XmlPullParser.END_TAG:
									tag = parser.getName();
									if(tag.equalsIgnoreCase("item")){
										itemClosed = true;
										peliculasDescargadas.add(pelicula);
									}
									break;
								}
								parserEvent = parser.next();
							}
						}
						break;
					}
					parserEvent = parser.next();
	
				}
			} catch (Exception e) {
				Log.e("Net", "Error in network call", e);
				error = true;
			}
			return peliculasDescargadas;
		}
		
		String imageSource=null;
	    class ImageGetter implements Html.ImageGetter {
	        public Drawable getDrawable(String source) {
	        	imageSource = source;
	        	return new BitmapDrawable();
	        }
	    };
	
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			peliculasAdapter.clear();
			progressDialog = ProgressDialog.show(MainActivity.this, getString(R.string.espere), getString(R.string.descargandopeliculas),true,true);
			progressDialog.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					tarea.cancel(true);
				}
			});
		}
	
		@Override
		protected void onCancelled() {
			super.onCancelled();
			peliculasAdapter.clear();
			peliculasDescargadas = new ArrayList<Pelicula>();
		}
	
		@Override
		protected void onProgressUpdate(String... progreso) {
			super.onProgressUpdate(progreso);
			progressDialog.setMessage(progreso[0]);
			peliculasAdapter.notifyDataSetChanged();
		}
	
		@Override
		protected void onPostExecute(List<Pelicula> result) {
			super.onPostExecute(result);
			for(Pelicula n:peliculasDescargadas){
				peliculasAdapter.add(n);
			}
			peliculasAdapter.notifyDataSetChanged();
			progressDialog.dismiss();
			if(error){
				Toast.makeText(context, R.string.errordered, Toast.LENGTH_LONG).show();
			}
			lanzaDescargaDeImagenes();
		}
	   
   }

   @SuppressWarnings("unchecked")
   void lanzaDescargaDeImagenes(){
		TareaDescargaImagen tdi = new TareaDescargaImagen();
		tdi.execute(peliculas);
   }

   
   private class TareaDescargaImagen extends AsyncTask<List<Pelicula>, String, Drawable>{

		@Override
		protected Drawable doInBackground(List<Pelicula>... arg0) {
			List<Pelicula> peliculas = arg0[0];
			int imagenesSinCargar = peliculas.size();
			while(imagenesSinCargar > 0){
				imagenesSinCargar = peliculas.size();
				for(Pelicula n:peliculas){
					if(n.getImagen()==null || n.getImagen().getIntrinsicHeight() <= 0){ // Reintento necesario?
						try{
							n.loadImagen(n.getLinkImagen());
							publishProgress("");
						}catch(Exception e){
							n.setImagen(getResources().getDrawable(R.drawable.ic_launcher));
						}
					}else{
						imagenesSinCargar --;
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
			return null;
		}
		@Override
		protected void onPostExecute(Drawable result) {
			super.onPostExecute(result);
			Toast.makeText(context, getString(R.string.imagenes_descargadas), Toast.LENGTH_SHORT).show();
			peliculasAdapter.notifyDataSetChanged();
		}
		
		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			peliculasAdapter.notifyDataSetChanged();
		}

	  
   }
   
   
}