package com.neurocreativa.peliculasestrenos;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/**
 * Esta aplicaci�n descarga las noticias RSS de www.tucamon.es y las muestra. 
 * Fue desarrollada en el taller de Android llevado a cabo en el aula de CAMON 
 * de Alicante en enero de 2011, por los asistentes y por Boy�n Bonev 
 * (Universidad de Alicante) Esta aplicaci�n no es \"oficial\" de CAMON, no se 
 * puede vender y su finalidad es did�ctica. La Universidad de Alicante y CAMON 
 * no se responsabilizan de su uso y modificaciones hechas por terceros.
 * 
 * @author Boyan Bonev, Universidad de Alicante
 *
 */
public class PeliculaAlertDialog extends AlertDialog.Builder {
	private Pelicula n;
	private Context c;
	private Speech s;

	/**
	 * After creating the NoticiaAlertDialog, don't forget to call .show();
	 * @param context The main Activity context.
	 * @param noticia Noticia object with information on Noticia
	 */
	protected PeliculaAlertDialog(Context context, Speech speech, Pelicula pelicula) {
		super(context);
		n = pelicula;
		c = context;
		s = speech;
		this.setTitle(n.getTitulo());
		this.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));
		this.setMessage(n.getDescripcion());
		this.setNegativeButton(context.getString(R.string.atras), null);
		this.setPositiveButton(context.getString(R.string.leemelo), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				s.speakAfterCheckingForTTS(n.getTitulo()+". "+n.getDescripcion());
			}
		});
		this.setNeutralButton(context.getString(R.string.web), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {  
				String input = n.getFecha();
				Date date = null;
				try {
					date = new SimpleDateFormat("dd-MM-yyyy").parse(input);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
				Intent intent = new Intent(Intent.ACTION_EDIT);
				intent.setType("vnd.android.cursor.item/event");
				intent.putExtra("beginTime", date.getTime());
				intent.putExtra("allDay", true);
				intent.putExtra("rrule", "FREQ=DAILY;COUNT=1");
				intent.putExtra("endTime", date.getTime()+60*60*1000);
				intent.putExtra("title", n.getTitulo());			
				c.startActivity( intent );
			}
		});
	}
}
