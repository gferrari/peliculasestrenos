package com.neurocreativa.peliculasestrenos;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PeliculasAdapter extends ArrayAdapter<Pelicula> {
	ArrayList<Pelicula> peliculas;
	Context context;

	public PeliculasAdapter(Context context, int textViewResourceId,
			ArrayList<Pelicula> peliculas) {
		super(context, textViewResourceId, peliculas);
		this.peliculas = peliculas;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.fila, null);
		}
		Pelicula pelicula = peliculas.get(position);
		if (pelicula != null) {
			TextView tv1 = (TextView) convertView.findViewById(R.id.FilaTexto1);
			TextView tv3 = (TextView) convertView.findViewById(R.id.FilaTexto3);				
			TextView tv2 = (TextView) convertView.findViewById(R.id.FilaTexto2);			
			ImageView iv = (ImageView) convertView
			.findViewById(R.id.FilaImagen);
			if (tv1 != null) {
				tv1.setText(pelicula.getTitulo());
			}
			if (tv3 != null) {
				tv3.setText(pelicula.getFecha());
			}			
			if (tv2 != null) {					
				tv2.setText(pelicula.getDescripcion());
			}
			if (iv != null) {
				iv.setImageDrawable(pelicula.getImagen());
			}
		}
		return convertView;
	}
}